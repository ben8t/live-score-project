"""helpers.py"""
import urllib.parse


def build_url(splash_endpoint, url, wait=0.5):
    return splash_endpoint + urllib.parse.urlencode({"url":url, "wait":0.5})
