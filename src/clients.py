"""clients.py"""
import requests
from lxml import html


class EurosportFootballClient:

    def process(self, url):
        response = requests.get(url)
        return self.parse(response)
        
    def parse(self, response):
        home_scores = []
        away_scores = []
        home_teams = []
        away_teams = []
        imgs = []

        tree = html.fromstring(response.content)
        for game in tree.cssselect('td.scoredata'):
            try:
                score = game.cssselect('a')[0].text.replace("\n", "").replace(" ", "")
                home_score = score[0]
                away_score = score[2]
            except:
                home_score = None
                away_score = None
            home_scores.append(home_score)
            away_scores.append(away_score)
        
        for game in tree.cssselect('td.first-team a'):
            home_teams.append(game.text)

        for game in tree.cssselect('td.second-team a'):
            away_teams.append(game.text)

        for game in tree.cssselect('td.fanion-column img'):
            imgs.append(game.attrib["src"])
        
        data = self.__build_formatted_data(home_scores, away_scores, home_teams, away_teams, imgs)
        return data


    def __build_formatted_data(self, home_scores, away_scores, home_teams, away_teams, imgs):
        data = []
        home_logos = [imgs[i] for i in range(0, len(imgs)) if i%2==0]
        away_logos = [imgs[i] for i in range(0, len(imgs)) if i%2!=0]
        for i in range(0, len(home_scores)):
            game = {}
            game["home_score"] = home_scores[i]
            game["away_score"] = away_scores[i]
            game["home_team"] = home_teams[i]
            game["away_team"] = away_teams[i]
            game["home_logo"] = home_logos[i]
            game["away_logo"] = away_logos[i]
            data.append(game)
        return data
