"""main.py"""
from flask import Flask
from flask import request, render_template
from src.clients import EurosportFootballClient
from src.helpers import build_url

app = Flask(__name__, template_folder="public", static_folder="public")

@app.route("/")
def home():
    client = EurosportFootballClient()
    splash_endpoint = "http://172.20.0.3:8050/render.html?"
    eurosport_url = "http://fr.livescore.eurosport.com/football/coupe-du-monde-feminine"
    url = build_url(splash_endpoint, eurosport_url)
    print(url)
    data = client.process(url)
    print(data)
    return render_template("index.html", data=data)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8082)